from socket import *

serverName = "localhost"
serverPort = 12000
brokenTCP = True  # True -> Il client non attende risposta dal server

character = "A"

clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName, serverPort))

for i in range(0, 100):
    clientSocket.send(character)

    if not brokenTCP:
        receivedString = clientSocket.recv(1024)
        print "Stringa spedita:", character, "| Stringa ricevuta:", receivedString

clientSocket.send(".")
clientSocket.close()
