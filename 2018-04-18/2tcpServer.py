from socket import *
from time import sleep

serverPort = 12000
brokenTCP = True  # True -> Il client non attende risposta dal server
fukinSlow = True  # True -> Attende un secondo per ricevere la risposta successiva

serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(("", serverPort))
serverSocket.listen(1)

while 1:
    connectionSocket, clientAddress = serverSocket.accept()
    toClose = False

    while not toClose:
        sentence = connectionSocket.recv(1024)

        if sentence == ".":
            toClose = True
            break

        if sentence.endswith("."):
            toClose = True
            sentence = sentence[:len(sentence) - 1]
        print "Stringa ricevuta:", sentence, "| Lunghezza:", len(sentence)

        if not brokenTCP:
            connectionSocket.send(sentence)

        if fukinSlow:
            sleep(1)

    connectionSocket.close()
    print "Connessione terminata"

serverSocket.close()