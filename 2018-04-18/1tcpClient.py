from socket import *

serverName = "localhost"  # Symbolic name -> [DNS] -> IP address
serverPort = 12000

close = "."
description = 'Inserisci una stringa in minuscolo o "' + close + '" per uscire: '

clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.setsockopt(SOL_TCP, TCP_NODELAY, 1)
clientSocket.connect((serverName, serverPort))

sentence = raw_input(description)
while sentence != close:
    clientSocket.send(sentence)

    modifiedSentence = clientSocket.recv(1024)

    print "Dal server:", modifiedSentence

    sentence = raw_input(description)

clientSocket.send(close)
clientSocket.close()