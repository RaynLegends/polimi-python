from socket import *
import thread


def handler(connection_socket):
    while True:
        sentence = connection_socket.recv(1024)

        if sentence == ".":
            break

        modified_sentence = sentence.upper()
        connection_socket.send(modified_sentence)

    connection_socket.close()


serverPort = 12000
close = "."

serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

serverSocket.bind(("", serverPort))
serverSocket.listen(1)  # Queue length

print "Server in attesa di connessioni"

while 1:
    connectionSocket, clientAddress = serverSocket.accept()
    thread.start_new_thread(handler, (connectionSocket,))
