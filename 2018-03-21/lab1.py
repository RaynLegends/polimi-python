import requests
import matplotlib.pyplot as plot

websites = ['http://www.google.it', 'http://www.polimi.it', 'http://www.netflix.com', 'http://www.reddit.com', 'http://twitter.com', 'http://arenacraft.it']
averages = []
maxes = 0
plot.figure()

for id_website, website in enumerate(websites):
    print "Testing", website, "with ID", id_website
    website_times = []
    for _ in range(10):
        request = requests.get(website)
        time = request.elapsed.microseconds / 1000
        website_times.append(time)
    maxes = max([maxes, max(website_times)])
    plot.plot(website_times, label=website)

    print "min", min(website_times), "ms"
    print "avg", sum(website_times) / len(website_times), "ms"
    print "max", max(website_times), "ms"
    averages.append(sum(website_times) / len(website_times))

print "best", websites[averages.index(min(averages))], min(averages)

plot.xlabel("Request ID")
plot.ylabel("[ms]")
plot.ylim([0, 1.1*maxes])
plot.title("Response time")
plot.legend(loc='lower right', fontsize=8)
plot.grid()
plot.show()
