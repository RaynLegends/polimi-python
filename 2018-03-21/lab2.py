import requests

websites = ['http://www.google.com',
        'http://www.youtube.com',
        'http://www.polimi.it',
        'http://www.wikipedia.org',
        'http://www.amazon.com',
        'http://www.twitter.com']

averages = []

for website in websites:
    print "Testing", website

    times = []
    for _ in range(10):
        request = requests.get(website)
        times.append(request.elapsed.microseconds / 1000)

    average = sum(times) / len(times)
    averages.append(sum)
    print average

print "Best", websites[averages.index(min(averages))], min(averages)
