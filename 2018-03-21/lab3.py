import socket
from geoip import geolite2

# DNS resolution
hostname = "www.google.it"
address = socket.gethostbyname(hostname)
print "The IP addres of", hostname, "is", address

# Country localization
match = geolite2.lookup(address)
if match is not None:
    print hostname, "is located in", match.country, match.continent
