from socket import *
from time import sleep
from threading import Thread

server_port = 12000
settings = [None]
clients_settings = dict()


def get_settings(client_address):
    if not client_address in clients_settings:
        clients_settings[client_address] = [None]

    return clients_settings[client_address]


def get_name(client_address):
    nickname = get_settings(client_address)[0]

    if nickname is not None:
        return nickname

    return str(client_address)


def set_name(client_address, nickname):
    get_settings(client_address)[0] = nickname


def server_thread():
    server_socket = socket(AF_INET, SOCK_DGRAM)
    server_socket.bind(('', server_port))
    print "Il server e' pronto a ricevere sulla porta " + str(server_port)

    while 1:
        client_message, client_address = server_socket.recvfrom(2048)

        if client_message.startswith('!'):
            if client_message.startswith('!nick'):
                nickname = client_message.split(' ')[1]
                set_name(client_address, nickname)
                print "Assegnato il nick", nickname, "a", client_address
                continue

        print get_name(client_address) + ": " + client_message
        # server_socket.sendto("[ACK]", client_address)


def client_thread():
    sleep(3)
    chat_host = raw_input('Inserisci indirizzo IP chat: ')
    chat_port = server_port
    chat_address = (chat_host, chat_port)

    client_socket = socket(AF_INET, SOCK_DGRAM)

    print "Il client e' pronto a inviare a " + str(chat_address)

    if settings[0] is not None:
        client_socket.sendto('!nick ' + settings[0], chat_address)

    message = raw_input('')
    while 1:
        if message.startswith('!'):
            if message.startswith('!nick'):
                settings[0] = raw_input('Inserisci il tuo nickname: ')
                client_socket.sendto('!nick ' + settings[0], chat_address)
            elif message.startswith('!quit'):
                print "Chat chiusa"
                break
            else:
                print "Comando non valido"
        else:
            client_socket.sendto(message, chat_address)
        message = raw_input('')

    client_socket.close()
    client_thread()


server_thread = Thread(target=server_thread)
server_thread.start()
client_thread()