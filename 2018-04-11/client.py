from socket import *

server_host = '10.8.0.9'
server_port = 12000
server_address = (server_host, server_port)

while 1:
    client_socket = socket(AF_INET, SOCK_DGRAM)
    message = raw_input('Inserisci una stringa in minuscolo: ')
    client_socket.sendto(message, server_address)

    client_socket.settimeout(2)
    try:
        result_message, result_address = client_socket.recvfrom(2048) # Already listening on the same port specified in sendto
        print result_message
    except socket.error as exception:
        print "Errore"
        print exception

client_socket.close()
