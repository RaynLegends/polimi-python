from socket import *

server_port = 12000
server_broken = False

server_socket = socket(AF_INET, SOCK_DGRAM)
server_socket.bind(('', server_port))
print "Il server e' pronto a ricevere"

while 1:
    if server_broken:
        continue

    client_message, client_address = server_socket.recvfrom(2048)
    print client_address
    print client_message
    result_message = client_message.upper()
    server_socket.sendto(result_message, client_address)

server_socket.close()